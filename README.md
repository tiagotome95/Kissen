# Kissen

Listen to music with Kissen.

## License

Copyright (C) 2019  Mara Vöcking, Tiago Simões Tomé

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

Kissen is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Description

## Get Started, Dependencies

Download and install Arduino IDE:
<https://www.arduino.cc/en/software>


**Libraries:**
* [DFRobotDFPlayerMini](https://github.com/DFRobot/DFRobotDFPlayerMini) by DFRobot
* SoftwareSerial
* Wire
* Servo
