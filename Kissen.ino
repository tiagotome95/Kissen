/* ###################################################
 * ###  Kissen3.13  #  16.09.2019, 23:20  #  äöüß  ###
 * ##################################################
 * ###  Authors: Tiago Simões Tomé, Mara Vöcking  ###
 * ##################################################
 * ###  ©2019  ###
 * ###############
 */
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#include <Servo.h>
#include <Wire.h>


const unsigned int Rx = 10;
const unsigned int Tx = 11;

SoftwareSerial softSerial(Rx, Tx);
DFRobotDFPlayerMini mp3Player;
const unsigned int maxSongs = 4;
unsigned int song = 1;
bool songState = false;

const int accGyAddr = 0x68;
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

const unsigned int PIR = 17;
bool PIRstate = LOW;      // definiert den aktuellen Wert des PIR-Sensors und setzt ihm auf LOW (AUS)
bool PIRlastState = LOW;  // definiert den letzten Wert des PIR-Sensors und setzt ihm auf LOW (AUS)

const unsigned int motorPin = 9;  // nur pin 9 oder 10 für Servo.h library
Servo motor;
signed int pos = 0;
bool posState = 0;

const unsigned int led = 13;
const unsigned int vibrator = 6;


unsigned long previousMillis0 = 0;    // definiert die letzte VergleichsMillisekunden der timerS Zeitfunktion
const unsigned long interval0 = 200;  // definiert den Zeitabstand für die Aktualisierung der seriellen Anzeige des Zeitzählers

unsigned long previousMicros1 = 0;    // definiert die letzte VergleichsMillisekunden der timerS Zeitfunktion
const unsigned long interval1 = 500;  // definiert den Zeitabstand für die Aktualisierung der seriellen Anzeige des Zeitzählers

unsigned long previousMillis2 = 0;    // definiert die letzte VergleichsMillisekunden der timerS Zeitfunktion
const unsigned long interval2 = 10000;  // definiert den Zeitabstand für die Aktualisierung der seriellen Anzeige des Zeitzählers

unsigned long previousMillisS = 0;    // definiert die letzte VergleichsMillisekunden der timerS Zeitfunktion
const unsigned long intervalS = 500;  // definiert den Zeitabstand für die Aktualisierung der seriellen Anzeige des Zeitzählers




void setup() {
  Serial.begin(9600);
  softSerial.begin(9600);

  if (!mp3Player.begin(softSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true){
      delay(0); // Code to compatible with ESP8266 watch dog.
    }
  }
  Serial.println(F("DFPlayer Mini online."));

  mp3Player.setTimeOut(500);
  mp3Player.volume(15);  // Lautstärke einstellen (0 bis 30)
  mp3Player.EQ(DFPLAYER_EQ_NORMAL);
  mp3Player.outputDevice(DFPLAYER_DEVICE_SD);
  //song = mp3Player.readCurrentFileNumber();
  mp3Player.play(1);




  Wire.begin();
  Wire.beginTransmission(accGyAddr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

  pinMode(PIR, INPUT);
  pinMode(vibrator, OUTPUT);
  motor.attach(motorPin);
}


void loop() {


  unsigned long currentMillis = millis();   // definiert die aktuelle Zeit in ms, seit dem letzten Start
  unsigned long currentMicros = micros();
  unsigned long timer0 = timer(currentMillis, previousMillis0);   // zählt die Zeit, seit das letzte mal als der PIR-Sensor HIGH war
  unsigned long timer1 = timer(currentMicros, previousMicros1);
  unsigned long timer2 = timer(currentMillis, previousMillis2);
  unsigned long timerS = timer(currentMillis, previousMillisS);

  if (timer0 >= interval0) {
    previousMillis0 = currentMillis;
    readAccGy();
    PIRstate = digitalRead(PIR);  // ermittelt den aktuellen Wert des PIR-Sensors

    showAccelerometer();
    showGyroscope();
    showTemperature();
  }

  if (timerS >= intervalS) {
    previousMillisS = currentMillis;

    showAccelerometer();
    //showGyroscope();
    //showTemperature();
  }


  if (AcZ < -12500) {
    if (PIRstate == HIGH && PIRlastState == LOW) {    // Wenn der PIR-Sensor HIGH wird, dann
      PIRlastState = HIGH;                            // wird der letzter Status des PIR-Sensors auf HIGH gesetzt,
      Serial.println("Bewegung erkannt!");
      previousMillis2 = currentMillis;
    }


    if (PIRlastState == HIGH) {
      Serial.println("PIR an");
      if (timer2 <= interval2) {
        if (timer1 >= interval1) {
          previousMicros1 = currentMicros;

          if (pos < 180 && posState == 0) {
            pos += 10;
          }
          if (pos == 180 && posState == 0) {
            posState = 1;
            digitalWrite(vibrator, HIGH);
          }
          if (pos > 0 && posState == 1) {
            pos -= 10;
          }
          if (pos == 0 && posState == 1) {
            posState = 0;
            digitalWrite(vibrator, LOW);
          }
          motor.write(pos);
          Serial.print("pos: "); Serial.println(pos);
          Serial.print("posState: "); Serial.println(posState);
        }
      }
    }

    if (timer2 >= interval2) {
      if (PIRstate == LOW && PIRlastState == HIGH) {    // Wenn der PIR-Sensor LOW wird, dann
        PIRlastState = LOW;                             // wird der letzter Status des PIR-Sensors auf LOW gesetzt,
        digitalWrite(vibrator, LOW);
      }
    }

    if (songState == true) {
      songState = false;
      mp3Player.pause();
    }

  }


  if (AcZ > 12500) {
    if (songState == false) {
      songState = true;
      mp3Player.next();
      /*
      if (song < maxSongs) {
        song++;
      }
      if (song == maxSongs) {
        song = 1;
      }
      mp3Player.play(song);
      */
    }

  }


}


unsigned long timer(unsigned long currentMillis, unsigned long previous) {
  unsigned long t = currentMillis - previous;
  return t;
}

void readAccGy() {
  Wire.beginTransmission(accGyAddr);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(accGyAddr, 14, true);
  AcX = Wire.read() << 8 | Wire.read();
  AcY = Wire.read() << 8 | Wire.read();
  AcZ = Wire.read() << 8 | Wire.read();
  Tmp = Wire.read() << 8 | Wire.read();
  GyX = Wire.read() << 8 | Wire.read();
  GyY = Wire.read() << 8 | Wire.read();
  GyZ = Wire.read() << 8 | Wire.read();
}

void showAccelerometer() {
  Serial.println("Accelerometer:");
  Serial.print("X: "); Serial.print(AcX);
  Serial.print("  ,  Y: "); Serial.print(AcY);
  Serial.print("  ,  Z: "); Serial.println(AcZ);
  Serial.println();
}

void showGyroscope() {
  Serial.println("Gyroscope:");
  Serial.print("X: "); Serial.print(GyX);
  Serial.print("  ,  Y: "); Serial.print(GyY);
  Serial.print("  ,  Z: "); Serial.println(GyZ);
  Serial.println();
}

void showTemperature() {
  Serial.println("Temperatur:");
  Serial.print("Tmp: "); Serial.println(Tmp/340.00+36.53);
  Serial.println();
}
